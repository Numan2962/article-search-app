import 'package:http/http.dart' as http;
import 'package:article_search_app/model/news_model.dart';

class ApiServices {
  var client = http.Client();

  Future<News> getNews(String query) async {
    var response = await client.get(Uri.parse(
        'https://newsapi.org/v2/everything?q=$query&apiKey=f8e81a5c12e14bd9b0f7b4b6258e2da8'));

    var jsnonString = response.body;
    return newsFromJson(jsnonString);
  }
}
