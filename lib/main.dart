import 'package:article_search_app/view/article_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Article Search App',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: ArticleSearch(
        category: '',
      ),
    );
  }
}
