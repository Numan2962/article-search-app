import 'package:article_search_app/controller/news_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ArticleView extends StatefulWidget {
  ArticleView(Type category);

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

class _ArticleViewState extends State<ArticleView> {
  NewsController n = Get.put(NewsController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Article Search App'),
      ),
      body: Obx(
        () => n.isLoading.value
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Container(
                child: ListView.builder(itemBuilder: (context, index) {
                  return Card(
                    child: Column(
                      children: [
                        Image.network(n.newsList[index].urlToImage),
                        Text(n.newsList[index].title),
                        SizedBox(
                          height: 8,
                        ),
                        Text(n.newsList[index].description),
                      ],
                    ),
                  );
                }),
              ),
      ),
    );
  }
}
