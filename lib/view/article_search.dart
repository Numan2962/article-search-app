import 'package:article_search_app/view/article_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ArticleSearch extends StatefulWidget {
  String category;
  ArticleSearch({required this.category});
  @override
  _ArticleSearchState createState() => _ArticleSearchState();
}

class _ArticleSearchState extends State<ArticleSearch> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Article Search App"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Image.asset('assets/images/logo.jpeg'),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.brown,
                    width: 4,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Search Articles', border: InputBorder.none),
                  onChanged: (newvalue) {
                    setState(() {
                      widget.category = newvalue;
                    });
                  },
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Get.to(ArticleView(Category), arguments: widget.category);
                },
                child: Icon(Icons.search),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
